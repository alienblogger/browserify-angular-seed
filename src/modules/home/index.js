import uiRouter from "angular-ui-router";

HomeController.$inject = [];
stateConfig.$inject = [
	"$stateProvider",
	"$urlRouterProvider",
	"$locationProvider"
];

function HomeController() {
	this.whoever = "hel!";
}

function stateConfig($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state("home", {
		url: "/home",
		template: require("./home.html"),
		controller: HomeController,
		controllerAs: "home"
	});
}

export default angular.module("home", [uiRouter]).config(stateConfig).name;
