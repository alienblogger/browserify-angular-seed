import angular from "angular";
import uiRouter from "angular-ui-router";
import uiBootstrap from "angular-ui-bootstrap";
import home from "./modules/home";

angular.module("app", [uiRouter, home]).config([
	"$locationProvider",
	"$urlRouterProvider",
	function($locationProvider, $urlRouterProvider) {
		$locationProvider.html5Mode(true);

		$urlRouterProvider.otherwise("/");
	}
]);
